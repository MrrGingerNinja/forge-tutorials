package mcforums;

public class Reference {
	public	static	final	String		MODID				= "MGN Forge";
	public	static	final	String		NAME				= "MrrGingerNinja's Forge Tutorials";
	public	static	final	String		VERSION				= "Tutorial #3";
	public	static	final	String		DEPENDENCIES 		= "required-after:Forge@[10.12.1.1217,)";
	public	static	final	String		MINECRAFT			= "[1.7.2,1.7.10,)";
}
