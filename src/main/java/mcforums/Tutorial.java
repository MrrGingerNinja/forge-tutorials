package mcforums;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod
	(
		modid						= Reference.MODID,
		name						= Reference.NAME,
		version						= Reference.VERSION,
		dependencies 				= Reference.DEPENDENCIES,
		acceptedMinecraftVersions	= Reference.MINECRAFT
	)

public class Tutorial {
	@Mod.Instance (value = Reference.MODID)
	public static Tutorial instance;
	
	@Mod.EventHandler
	public void preInitialize (FMLPreInitializationEvent event) {
		
	}
	
	@Mod.EventHandler
	public void initialize (FMLInitializationEvent event) {
		
	}
	
	@Mod.EventHandler
	public void postInitialization (FMLPostInitializationEvent event) {
		
	}
}
