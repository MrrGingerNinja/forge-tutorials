MrrGingerNinja's Forge Modding Tutorials
========================================

View in Raw mode for more organised layout

----------------------------------------

Tutorial #4		-	Items and Blocks
					-	COMING SOON

Tutorial #3		-	The Basics of 1.7.x Mod Creation
					-	Setting up the skeleton of our mod file
					-	Introduces 4 imports that you will recognise from 1.6.x modding:
						-	Mod
						-	FMLPreInitializationEvent
						-	FMLInitializationEvent
						-	FMLPostInitializationEvent
					-	@NetworkMod is no longer available for Forge 1.7.x

Tutorial #2		-	Exporting our Mod
					-	Setting up Gradle within the System variables
					-	Creating a build.gradle file
					-	Use command line 'gradle build' to pack our mod into a .jar file

Tutorial #1		-	JDK, Eclipse, Forge and Gradle
					-	Step-by-step guide to downloading Java and setting up the Java Development Kit
					-	An installation guide for Forge 1.7.x using the new 'gradlew' file
					-	Setting up Forge as a project in Eclipse and creating an empty project for our mod